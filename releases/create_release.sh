#!/bin/bash

if [[ -z "$1" ]]; then

	echo "Usage: $0 {version_number}"
else
	# Copy compiled library and header file to the prefered libith directory structure
	cp -rpvf ../dist/default/production/libtih.X.a libtih/lib/libtih.a
	cp -rpvf ../libtih.h libtih/include/libtih.h

	# Create compressed archives, both zip and tar.gz so people can choose.
	tar -zxvf libtih_v"$1".tar.gz libtih
	zip -rv libtih_v"$1".zip libtih
fi
