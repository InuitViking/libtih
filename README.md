# libtih

A library for the school project [The Intelligent House](https://gitlab.com/InuitViking/the-intelligent-house).

MPLAB X IDE was used to initialize the project, if that is important to know.

## Latest release
The latest release can be found here:
- [libtih_v1.0.tar.gz](releases/libtih_v1.0.tar.gz)
- [libtih_v1.0.zip](releases/libtih_v1.0.zip)


## How to include
### Prerequisites
1. Create a lib directory in your project if you haven't already. We'll use `libs` in this example
2. Download a release and uncompress and extract it into `libs`.

### MPLAB X IDE
1. In `Project` tab under `Libraries`:
    - Right click `Libraries`
    - Click `Add Library/Object File...`
    - Find `libs/libtih/lib/libtih.a`
2. In `Project` tab under `Header Files`:
    - Right click `Header Files`.
    - Click `Add Existing Item...`
    - Find `libs/libtih/include/libtih.h`

## Functions
### ADC_init
Initializes analog port 0, A0 on the Arduino board

#### Usage:
```c
ADC_init();
```

### Timer_Microsecond
Sets a pretty accurate hardware timer (according to the provided clock_frequency) in microseconds.

It is more accurate than the software delays specified in utils/delay.h.

#### Usage:
```c
microseconds = 1000;

Timer_Microsecond(microseconds, F_CPU);
```

### Timer_Millisecond
Sets a pretty accurate hardware timer (according to the provided clock_frequency) in milliseconds.

It is more accurate than the software delays specified in utils/delay.h.

#### Usage:
```c
milliseconds = 1000;

Timer_Microsecond(milliseconds, F_CPU);
```
