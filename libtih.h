/* 
 * File:   libtih.h
 * Author: ch
 *
 * Created on December 7, 2020, 12:01 PM
 */

/*
 * Macros
 */
#define F_CPU 16000000UL
#define BAUD 9600
#define BAUD_TOL 2

/*
 * Functions
 */

/*
 * * * * * * * * * * * * * * * * * *
 * ADC 	                           *
 * * * * * * * * * * * * * * * * * *
 */

/**
 * This function initializes analog port 0, A0 on the Arduino board
 */
void ADC_init(void);

/*
 * * * * * * * * * * * * * * * * * *
 * UART	                           *
 * * * * * * * * * * * * * * * * * *
 */

#ifndef STDIO_SETUP_H_
#define STDIO_SETUP_H_

/**
 * Initializes the UART, making it possible to read through a console
 */
void UartInit(void);

#endif /* STDIO_SETUP_H_ */

/*
 * * * * * * * * * * * * * * * * * *
 * Timers                          *
 * * * * * * * * * * * * * * * * * *
 */

/*
 * It may be noted that these are only accurate to a certain point.
 * Expect some minor inaccuracies in microseconds.
 * If you happen to have an analyzer, you can easily adjust to the inaccuracies.
 */

/**
 * Sets a pretty accurate hardware timer (according to the provided clock_frequency) in microseconds.
 * It is more accurate than the software delays specified in utils/delay.h.
 * 
 * @param microseconds
 * @param clock_frequency
 */
void Timer_Microsecond(float microseconds, long clock_frequency);

/**
 * Sets a pretty accurate hardware timer (according to the provided clock_frequency) in milliseconds.
 * It is more accurate than the software delays specified in utils/delay.h.
 * 
 * @param milliseconds
 * @param clock_frequency
 */
void Timer_Millisecond(float milliseconds, long clock_frequency);