/* 
 * File:   libtih.c
 * Author: Angutivik Casper Rúnur Tausen
 *
 * Created on December 7, 2020, 12:01 PM
 */

/*
 * Macros
 */
#define F_CPU 16000000UL

/*
 * System headers
 */
#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdio.h>

/*
 * Functions
 */

/*
 * * * * * * * * * * * * * * * * * *
 * ADC 	                           *
 * * * * * * * * * * * * * * * * * *
 */

/**
 * This function initializes analog port 0, A0 on the Arduino board
 */
void ADC_init(void){
	ADMUX = (1 << REFS0);	// 5V supply used for ADC reference, select ADC channel 0, datasheet 28.9.1
	DIDR0 = (1 << ADC0D);	// disable digital input on ADC0 pin, datasheet 28.9.6
	// enable ADC, start ADC, ADC clock = 16MHz / 128 = 125kHz, datasheet 28.9.2
	ADCSRA = (1<<ADEN)|(1<<ADSC)|(1<<ADATE)|(1<<ADIE)|(1<<ADPS2)|(1<<ADPS1)|(1<<ADPS0);
	sei();
}

/*
 * * * * * * * * * * * * * * * * * *
 * Timers                          *
 * * * * * * * * * * * * * * * * * *
 */

/*
 * It may be noted that these are only accurate to a certain point.
 * Expect some minor inaccuracies in microseconds.
 * If you happen to have an analyzer, you can easily adjust to the inaccuracies.
 */

/**
 * Sets a pretty accurate hardware timer (according to the provided clock_frequency) in microseconds.
 * It is more accurate than the software delays specified in utils/delay.h.
 * 
 * @param microseconds
 * @param clock_frequency
 */
void Timer_Microsecond(float microseconds, long clock_frequency){
	TCCR1B	|=	(1<<WGM12);														// Mode 4, CTC = WGM10 - WGM13 = 0100, Table 20-6 (page 176)
	TIMSK1	|=	(1<<OCIE1A);													// Timer/Counter1, Output Compare A Match Interrupt Enable, 20.15.8 (page 184) 		
	OCR1A	=	((microseconds * (clock_frequency / 1000) / 256) - 1) / 1000;	// OCR1A = (Tdelay * F_CPU / N) - 1 => 61.5 gives 1ms => 62499 gives 1s
	TCCR1B	|=	(1<<CS12);														// Prescaler: 256, CS=100, Table 20-7 (page 177). Timer starts!
}

/**
 * Sets a pretty accurate hardware timer (according to the provided clock_frequency) in milliseconds.
 * It is more accurate than the software delays specified in utils/delay.h.
 * 
 * @param milliseconds
 * @param clock_frequency
 */
void Timer_Millisecond(float milliseconds, long clock_frequency){
	TCCR1B	|= (1<<WGM12);														// Mode 4, CTC = WGM10 - WGM13 = 0100, Table 20-6 (page 176)
	TIMSK1	|= (1<<OCIE1A);														// Timer/Counter1, Output Compare A Match Interrupt Enable, 20.15.8 (page 184) 		
	OCR1A	= (milliseconds * (clock_frequency / 1000) / 256) - 1;				// OCR1A = (Tdelay * F_CPU / N) - 1 => 61.5 gives 1ms => 62499 gives 1s
	TCCR1B	|= (1<<CS12);														// Prescaler: 256, CS=100, Table 20-7 (page 177). Timer starts!
}